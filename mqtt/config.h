/* config.h
*
* Copyright (c) 2014-2015, Tuan PM <tuanpm at live dot com>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* * Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
* * Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the distribution.
* * Neither the name of Redis nor the names of its contributors may be used
* to endorse or promote products derived from this software without
* specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef USER_CONFIG_H_
#define USER_CONFIG_H_
#include "os_type.h"
#include "user_config.h"
typedef struct{
	uint32_t cfg_holder;

	uint8_t stationSsid[64];
	uint8_t stationPwd[64];
	uint32_t stationType;

	uint8_t deviceId[64];
	uint8_t mqttHost[64];
	uint32_t mqttPort;
	uint8_t mqttUser[32];
	uint8_t mqttPass[32];
	uint8_t description[64];
	uint8_t attribute[64]; // Unit attributes

	uint8_t webUser[32];
	uint8_t webPassword[32];
	uint16_t enableWebPageControl;

	uint32_t mqttKeepalive;

	uint16_t dawn;
	uint16_t dusk;
	uint8_t peak;
	uint8_t offPeak;
	uint8_t frost;
	uint16_t onOne;
	uint16_t offOne;
	uint16_t onTwo;
	uint16_t offTwo;
	uint8_t sensor;
	uint8_t out0Status;
	uint8_t out4Status;
	uint8_t out5Status;
	uint8_t out12Status;
	uint8_t out13Status;
	uint8_t in14Bounce;
	uint8_t enable13;
	uint8_t override0;
	uint32_t override0Time;
	uint8_t base[32];
	uint8_t invert;
	uint8_t security; // was it interfering with dawn/dusk?
	uint16_t calibrate; // ADC Calibration
	uint8_t wifiSetup; // reboot and go into setup mode
	uint8_t mqttSubscribe[4][32];
	uint8_t tcpAddress[4];
	uint8_t tcpGateway[4];
	uint8_t tcpNetmask[4];
	uint16_t checksum;
	uint8_t clock;
	uint8_t out2Status;
	uint8_t out15Status;
	uint8_t lastFail;

	uint8_t otaHost[64];
	uint32_t otaPort;
	uint8_t serial2;

} SYSCFG;

typedef struct {
    uint8 flag;
    uint8 pad[3];
} SAVE_FLAG;


void ICACHE_FLASH_ATTR cfgSave();
void ICACHE_FLASH_ATTR cfgLoad();

extern SYSCFG sysCfg;

#endif /* USER_CONFIG_H_ */
