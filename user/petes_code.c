/*
 * petes_code.c
 *
 *  Created on: July - September 2015
 *      Authors: Aidan and Peter (this page Peter Scargill - http://tech.scargill.net)
 */

#include "aidan_and_petes.h"
#include "petes.h"
#include "easygpio/easygpio.h"

#include "rboot-ota.h"
#include "softuart.h"

#define RGBMAX 200
#define TICKS 6000 // REBOOTING NOT GOOD - 4 secs a count - messes up IO so only after a LONG time of no WIFI do we reboot

int16_t  heartBeat=TICKS-1; // ticks before forcing a reset with GPIO16
uint8_t connectStatus=0; // i.e. what status are we in - do we need to connect MQTT etc?

Softuart softuart2;

// connectStatus - see losttheplot callback
// 1 means got WIFI
// 2 means initialised timers
// 4 means subscribed to MQTT
// 8 means WIFI has connected at least once since powerup
// 16 means do above - if reset do nothing as unit is in wifi setup mode (see aidans_code.c)

int16_t rgbPlayBuffer[RGBMAX];
int16_t rgbPlayPtr=0;
int16_t rgbRecordPtr=0;
int8_t  rgbDoSeq=0;
int16_t rgbTotal=60;
int8_t rgbOnce=0;
uint16_t out0Timer=0;

int8_t e13OnTime=10;
int8_t e13OffTime=10;
int8_t e13OnSet=1;
int8_t e13OffSet=19;


// experiment

static void ICACHE_FLASH_ATTR OtaUpdate_CallBack(bool result, uint8 rom_slot) {

	if(result == true) {
		// success
		if (rom_slot == FLASH_BY_ADDR) {
			iprintf(INFO, "Write successful.\r\n");
		} else {
			// set to boot new rom and then reboot
			iprintf(INFO, "Firmware updated, rebooting to rom %d...\r\n", rom_slot);
			rboot_set_current_rom(rom_slot);
			system_restart();
		}
	} else {
		// fail
		iprintf(INFO, "Firmware update failed!\r\n");
	}
}

static void ICACHE_FLASH_ATTR OtaUpdate() {

	// start the upgrade process
	if (rboot_ota_start((ota_callback)OtaUpdate_CallBack)) {
		iprintf(INFO, "Updating...\r\n");
	} else {
		iprintf(INFO, "Updating failed!\r\n\r\n");
	}

}

// end of experiment

static uint8_t wsBitMask;

static const uint8_t ICACHE_RODATA_ATTR  kelvin[] = {
	    255,  51,   0, //    1000
	    255, 109,   0, //    1500
	    255, 137,  18, //    2000
	    255, 161,  72, //    2500
	    255, 180, 107, //    3000
	    255, 196, 137, //    3500
	    255, 209, 163, //    4000
	    255, 219, 186, //    4500
	    255, 228, 206, //    5000
	    255, 236, 224, //    5500
	    255, 243, 239, //    6000
	    255, 249, 253, //    6500
	    245, 243, 255, //    7000
	    235, 238, 255, //    7500
	    227, 233, 255, //    8000
	    220, 229, 255, //    8500
	    214, 225, 255, //    9000
	    208, 222, 255, //    9500
	    204, 219, 255, //    10000
	    200, 217, 255, //    10500
	    196, 215, 255, //    11000
	    193, 213, 255, //    11500
	    191, 211, 255, //    12000
	    188, 210, 255, //    12500
	    186, 208, 255, //    13000
	    184, 207, 255, //    13500
	    182, 206, 255, //    14000
	    180, 205, 255, //    14500
	    179, 204, 255, //    15000
	    177, 203, 255, //    15500
	    176, 202, 255, //    16000
	    175, 201, 255, //    16500
	    174, 200, 255, //    17000
	    173, 200, 255, //    17500
	    172, 199, 255, //    18000
	    171, 198, 255, //    18500
	    170, 198, 255, //    19000
	    169, 197, 255, //    19500
	    168, 197, 255, //    20000
	    168, 196, 255, //    20500
	    167, 196, 255, //    21000
	    166, 195, 255, //    21500
	    166, 195, 255, //    22000
	    165, 195, 255, //    22500
	    164, 194, 255, //    23000
	    164, 194, 255, //    23500
	    163, 194, 255, //    24000
	    163, 193, 255, //    24500
	    163, 193, 255, //    25000
	    162, 193, 255, //    25500
	    162, 192, 255, //    26000
	    161, 192, 255, //    26500
	    161, 192, 255, //    27000
	    161, 192, 255, //    27500
	    160, 191, 255, //    28000
	    160, 191, 255, //    28500
	    160, 191, 255, //    29000
	    159, 191, 255, //    29500
	    159, 191, 255, //    30000
	    159, 190, 255, //    30500
	    159, 190, 255, //    31000
	    158, 190, 255, //    31500
	    158, 190, 255, //    32000
	    158, 190, 255, //    32500
	    158, 190, 255, //    33000
	    157, 189, 255, //    33500
	    157, 189, 255, //    34000
	    157, 189, 255, //    34500
	    157, 189, 255, //    35000
	    157, 189, 255, //    35500
	    156, 189, 255, //    36000
	    156, 189, 255, //    36500
	    156, 189, 255, //    37000
	    156, 188, 255, //    37500
	    156, 188, 255, //    38000
	    155, 188, 255, //    38500
	    155, 188, 255, //    39000
	    155, 188, 255, //    39500
	    155, 188, 255  //    40000
	    };

// this can be a uint8_t table if you take it out of FLASH.
static const uint8_t ICACHE_RODATA_ATTR  ledTable[256] =
			{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4,
					4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 9, 9, 9, 10, 10, 10, 11, 11, 12, 12, 12, 13, 13, 14, 14, 15, 15, 15, 16, 16, 17, 17, 18,
					18, 19, 19, 20, 20, 21, 22, 22, 23, 23, 24, 25, 25, 26, 26, 27, 28, 28, 29, 30, 30, 31, 32, 33, 33, 34, 35, 36, 36, 37, 38, 39, 40, 40, 41,
					42, 43, 44, 45, 46, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67, 68, 69, 70, 71, 72, 73, 75, 76, 77,
					78, 80, 81, 82, 83, 85, 86, 87, 89, 90, 91, 93, 94, 95, 97, 98, 99, 101, 102, 104, 105, 107, 108, 110, 111, 113, 114, 116, 117, 119, 121,
					122, 124, 125, 127, 129, 130, 132, 134, 135, 137, 139, 141, 142, 144, 146, 148, 150, 151, 153, 155, 157, 159, 161, 163, 165, 166, 168, 170,
					172, 174, 176, 178, 180, 182, 184, 186, 189, 191, 193, 195, 197, 199, 201, 204, 206, 208, 210, 212, 215, 217, 219, 221, 224, 226, 228, 231,
					233, 235, 238, 240, 243, 245, 248, 250, 253, 255 };

// this can be a uint16_t table if you take it out of FLASH.
static const uint32_t ICACHE_RODATA_ATTR PWMTable[100] =
	{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 24, 26, 28, 30, 34, 38, 42, 47, 54, 69, 84, 104, 124, 154, 184, 224, 264, 314, 364, 439, 518, 618,718, 868, 1108, 1308, 1508, 1758,
			1950, 2150, 2350, 2550, 2770, 2950, 3200, 3500, 3800, 4100, 4400, 4700, 5000, 5400, 5800, 6200, 6600, 7000, 7500, 8000, 8500, 9000, 9500, 10000, 10500, 11100, 11500, 12000, 12500, 13000, 13500, 14000, 14500, 15000, 15500,
			16000, 16500, 17000, 17500, 18000, 18500, 19000, 19500, 20000, 20500, 21000, 21500, 22000, 22500, 23000};

//
// This routine courtesy Richard A Burton - way better than using 32 bit flash arrays (you can't directly
// access 8 bit FLASH arrays - will crash the processor)
uint8 ICACHE_FLASH_ATTR read_rom_uint8(const uint8* addr){
    uint32 bytes;
    bytes = *(uint32*)((uint32)addr & ~3);
    return ((uint8*)&bytes)[(uint32)addr & 3];
}

#define PWM_0_OUT_IO_MUX PERIPHS_IO_MUX_MTDI_U
#define PWM_0_OUT_IO_NUM 12
#define PWM_0_OUT_IO_FUNC FUNC_GPIO12
#define PWM_1_OUT_IO_MUX PERIPHS_IO_MUX_MTDO_U
#define PWM_1_OUT_IO_NUM 15
#define PWM_1_OUT_IO_FUNC FUNC_GPIO15

#define PWM_2_OUT_IO_MUX PERIPHS_IO_MUX_MTCK_U
#define PWM_2_OUT_IO_NUM 13
#define PWM_2_OUT_IO_FUNC FUNC_GPIO13


#define PWM_3_OUT_IO_MUX PERIPHS_IO_MUX_GPIO4_U
#define PWM_3_OUT_IO_NUM 4
#define PWM_3_OUT_IO_FUNC FUNC_GPIO4
#define PWM_4_OUT_IO_MUX PERIPHS_IO_MUX_GPIO5_U
#define PWM_4_OUT_IO_NUM 5
#define PWM_4_OUT_IO_FUNC FUNC_GPIO5
#define PWM_CHANNEL 5

uint32 io_info[][3]={
{PWM_1_OUT_IO_MUX,PWM_1_OUT_IO_FUNC,PWM_1_OUT_IO_NUM},
{PWM_3_OUT_IO_MUX,PWM_3_OUT_IO_FUNC,PWM_3_OUT_IO_NUM},
{PWM_4_OUT_IO_MUX,PWM_4_OUT_IO_FUNC,PWM_4_OUT_IO_NUM}
};

uint8 dummy[]= {1,2,3,4,5,6,7,8};
typedef struct
	{
		uint16_t reda;
		uint16_t greena;
		uint16_t bluea;
		uint16_t red;
		uint16_t green;
		uint16_t blue;
	} LEDX;
LEDX pwm_array;

typedef struct
	{
		uint8_t device;
		uint16_t active;
		uint16_t activeCounter;
		uint16_t gap;
		uint16_t gapCounter;
		uint16_t repeats;
	} MIST;
MIST mist;

uint32 duty[]= {0, 0, 0};
uint32 freq = 1000;
static uint8 donepwm=0;

void ICACHE_FLASH_ATTR mqttConnectedCb(uint32_t *args)
    {
    char baseBuf[TBUFSIZE];
    connectStatus|=4;
    MQTT_Client* client = (MQTT_Client*) args;
    iprintf(INFO,"MQTT: Connected Device: %s\r\n", sysCfg.base);
    MQTT_Subscribe(client, "toesp", 0);
    os_sprintf(baseBuf, "%s/toesp", sysCfg.base);

    MQTT_Subscribe(client, baseBuf, 0);
    os_sprintf(baseBuf, "{\"id\":\"%s\",\"desc\":\"%s\",\"attribute\":\"%s\",\"fail\":\"%d\"}",
	    sysCfg.base, sysCfg.description, sysCfg.attribute, sysCfg.lastFail); // changed to json
    MQTT_Publish(&mqttClient, "esplogon", baseBuf, strlen(baseBuf), 0, 0);
    }


void ICACHE_FLASH_ATTR mqttDisconnectedCb(uint32_t *args)
	{
	    connectStatus&=(255-4); // note disconnection to prevent repeats
		MQTT_Client* client = (MQTT_Client*) args;
		iprintf(INFO,"MQTT: Disconnected\r\n");
	}

void ICACHE_FLASH_ATTR mqttPublishedCb(uint32_t *args)
	{
		MQTT_Client* client = (MQTT_Client*) args;
		iprintf(INFO,"MQTT: Published\r\n");
	}

/************************* Partial Arduino time library with bits added ******/
typedef struct
	{
		uint8_t Second;
		uint8_t Minute;
		uint8_t Hour;
		uint8_t Wday;     // day of week, Sunday is day 1
		uint8_t Day;
		uint8_t Month;
		uint16_t Year;
		unsigned long Valid;
	} tm_t;

tm_t tm;

#define LEAP_YEAR(Y)     ( ((1970+Y)>0) && !((1970+Y)%4) && ( ((1970+Y)%100) || !((1970+Y)%400) ) )

static const uint8_t monthDays[] =
	{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };     // API starts months from 1, this array starts from 0

void ICACHE_FLASH_ATTR convertTime()
	{
		// given myrtc as time in Linux format break into time components
		// this is a more compact version of the C library localtime function
		// note that internally the year is offset from 1970 - compensated at the end

		uint8_t year;
		uint8_t month, monthLength;
		uint32_t time;
		unsigned long days;

		time = (uint32_t) myRtc;
		tm.Second = time % 60;
		time /= 60;     // now it is minutes
		tm.Minute = time % 60;
		time /= 60;     // now it is hours
		tm.Hour = time % 24;
		time /= 24;     // now it is days
		tm.Wday = ((time + 4) % 7) + 1;     // Sunday is day 1

		year = 0;
		days = 0;
		while ((unsigned) (days += (LEAP_YEAR(year) ? 366 : 365)) <= time) {
			year++;
		}
		tm.Year = year;     // year is offset from 1970

		days -= LEAP_YEAR(year) ? 366 : 365;
		time -= days;     // now it is days in this year, starting at 0

		days = 0;
		month = 0;
		monthLength = 0;
		for (month = 0; month < 12; month++) {
			if (month == 1) {     // february
				if (LEAP_YEAR(year)) {
					monthLength = 29;
				}
				else {
					monthLength = 28;
				}
			}
			else {
				monthLength = monthDays[month];
			}

			if (time >= monthLength) {
				time -= monthLength;
			}
			else {
				break;
			}
		}
		tm.Month = month + 1;     // jan is month 1
		tm.Day = time + 1;     // day of month
		tm.Year = tm.Year + 1970;
	}

/* end time */

#define MAXTIMINGS 10000
#define BREAKTIME 20

static void ICACHE_FLASH_ATTR readDHT()
	{
		int counter = 0;
		int laststate = 1;
		int i = 0;
		int j = 0;
		int checksum = 0;
		//int bitidx = 0;
		//int bits[250];

		int data[100];

		data[0] = data[1] = data[2] = data[3] = data[4] = 0;
		easygpio_pinMode(2, EASYGPIO_PULLUP, EASYGPIO_OUTPUT);
		easygpio_outputSet(2, 1);
		os_delay_us(250000);
		easygpio_outputSet(2, 0);
		os_delay_us(20000);
		easygpio_outputSet(2, 1);
		os_delay_us(40);
		easygpio_outputDisable(2);

		// wait for pin to drop?
		while (easygpio_inputGet(2) == 1 && i < 100000) {
			os_delay_us(1);
			i++;
		}
		if (i == 100000) return;

		// read data!

		for (i = 0; i < MAXTIMINGS; i++) {
			counter = 0;
			while ( easygpio_inputGet(2) == laststate) {
				counter++;
				os_delay_us(1);
				if (counter == 1000) break;
			}
			laststate = easygpio_inputGet(2);
			if (counter == 1000) break;

			//bits[bitidx++] = counter;

			if ((i > 3) && (i % 2 == 0)) {
				// shove each bit into the storage bytes
				data[j / 8] <<= 1;
				if (counter > BREAKTIME) data[j / 8] |= 1;
				j++;
			}
		}

		float temp_p, hum_p;
		if (j >= 39) {
			checksum = (data[0] + data[1] + data[2] + data[3]) & 0xFF;
			if (data[4] == checksum) {
				/* yay! checksum is valid */

			if (sysCfg.sensor==2)
		{
				hum_p = data[0];
				temp_p = data[2];

		}
			else
		{
				hum_p = data[0] * 256 + data[1];
				hum_p /= 10;

				temp_p = (data[2] & 0x7F) * 256 + data[3];
				temp_p /= 10.0;
				if (data[2] & 0x80) temp_p *= -1;

		}


				temperature = temp_p;
				humidity = hum_p;
			}
		}
	}

/*
 * Adaptation of Paul Stoffregen's One wire library to the ESP8266 and
 * Necromant's Frankenstein firmware by Erland Lewin <erland@lewin.nu>
 *
 * Paul's original library site:
 *   http://www.pjrc.com/teensy/td_libs_OneWire.html
 *
 * See also http://playground.arduino.cc/Learning/OneWire
 *
 * Stripped down to bare minimum by Peter Scargill for single DS18B20 or DS18B20P integer read
 */

static int gpioPin=2;

// Perform the onewire reset function.  We will wait up to 250uS for
// the bus to come high, if it doesn't then it is broken or shorted
// and we return;

void ICACHE_FLASH_ATTR ds_reset(void)
	{
		uint8_t retries = 125;
		easygpio_pinMode(2, EASYGPIO_PULLUP, EASYGPIO_INPUT);
		// wait until the wire is high... just in case
		do {
			if (--retries == 0) return;
			os_delay_us(2);
		} while (!easygpio_inputGet(gpioPin));
		easygpio_outputEnable(gpioPin,0);
		os_delay_us(480);
		easygpio_outputDisable(gpioPin);
		os_delay_us(480);
	}

// Write a bit. Port and bit is used to cut lookup time and provide
// more certain timing.
//
static ICACHE_FLASH_ATTR inline void write_bit(int v)
	{
		easygpio_outputEnable(gpioPin,0);
		//easygpio_outputSet(gpioPin, 0);
		if (v) {
			os_delay_us(10);
			easygpio_outputSet(gpioPin, 1);
			os_delay_us(55);
		}
		else {
			os_delay_us(65);
			easygpio_outputSet(gpioPin, 1);
			os_delay_us(5);
		}
	}

//
// Read a bit. Port and bit is used to cut lookup time and provide
// more certain timing.
//
static ICACHE_FLASH_ATTR inline int read_bit(void)
	{
		int r;
		easygpio_outputEnable(gpioPin,0);
		os_delay_us(3);
		easygpio_outputDisable(gpioPin);
		os_delay_us(10);
		r = easygpio_inputGet(gpioPin);
		os_delay_us(53);
		return r;
	}

//
// Write a byte. The writing code uses the active drivers to raise the
// pin high, if you need power after the write (e.g. DS18S20 in
// parasite power mode) then set 'power' to 1, otherwise the pin will
// go tri-state at the end of the write to avoid heating in a short or
// other mishap.
//
void ICACHE_FLASH_ATTR ds_write(uint8_t v, int power)
	{
		uint8_t bitMask;
		for (bitMask = 0x01; bitMask; bitMask <<= 1) {
			write_bit((bitMask & v) ? 1 : 0);
		}
		if (!power) {
			easygpio_outputDisable(gpioPin);
			easygpio_outputSet(gpioPin, 0);
		}
	}

//
// Read a byte
//
uint8_t ICACHE_FLASH_ATTR ds_read()
	{
		uint8_t bitMask;
		uint8_t r = 0;
		for (bitMask = 0x01; bitMask; bitMask <<= 1) {
			if (read_bit()) r |= bitMask;
		}
		return r;
	}

// ----------------------------------------------------------------------------
// -- This WS2812 code must be compiled with -O2 to get the timing right.  Read this:
// -- http://wp.josh.com/2014/05/13/ws2812-neopixels-are-not-so-finicky-once-you-get-to-know-them/
// -- The ICACHE_FLASH_ATTR is there to trick the compiler and get the very first pulse width correct.
static void  __attribute__((optimize("O2"))) send_ws_0(uint8_t gpio){
  uint8_t i;
  i = 4; while (i--) GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 1 << gpio);
  i = 9; while (i--) GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 1 << gpio);
}

static void __attribute__((optimize("O2"))) send_ws_1(uint8_t gpio){
  uint8_t i;
  i = 8; while (i--) GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 1 << gpio);
  i = 6; while (i--) GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 1 << gpio);
}


void ICACHE_FLASH_ATTR WS2812OutBuffer(uint8_t * buffer, uint16_t length,uint16_t repetition)
    {
    uint16_t i;
    if (wsBitMask>16) return;
   ets_intr_lock();

    for (i = 0; i < 2; i++)
	WRITE_PERI_REG(PERIPHS_GPIO_BASEADDR + 8, 1<< wsBitMask);
    while (repetition--)
	{
	for (i = 0; i < length; i++)
	    {
	    uint8_t mask = 0x80;
	    uint8_t byte = buffer[i];
	    while (mask)
		{
		(byte & mask) ? send_ws_1(wsBitMask) : send_ws_0(wsBitMask);

	    mask >>= 1;
		}
	    }
	}
    ets_intr_unlock();
    }

void ICACHE_FLASH_ATTR ok()
	{
	iprintf(RESPONSE,"Ok\r\n");
	}

// experimental HSV to RGB
struct RGB_set {
 unsigned char r;
 unsigned char g;
 unsigned char b;
} RGB_set;

struct HSV_set {
 signed int h;
 unsigned char s;
 unsigned char v;
} HSV_set;

int inline fasterfloor( const float x ) { return x < 0 ? (int) x == x ? (int) x : (int) x -1 : (int) x; }
/*******************************************************************************
 * Function HSV2RGB
 * Description: Converts an HSV color value into its equivalen in the RGB color space.
 * Copyright 2010 by George Ruinelli
 * The code I used as a source is from http://www.cs.rit.edu/~ncs/color/t_convert.html
 * Parameters:
 *   1. struct with HSV color (source)
 *   2. pointer to struct RGB color (target)
 * Notes:
 *   - r, g, b values are from 0..255
 *   - h = [0,360], s = [0,255], v = [0,255]
 *   - NB: if s == 0, then h = 0 (undefined)
 ******************************************************************************/
void ICACHE_FLASH_ATTR HSV2RGB(struct HSV_set HSV, struct RGB_set *RGB){
 int i;
 float f, p, q, t, h, s, v;

 h=(float)HSV.h;
 s=(float)HSV.s;
 v=(float)HSV.v;

 s /=255;

 if( s == 0 ) { // achromatic (grey)
 RGB->r = RGB->g = RGB->b = v;
 return;
 }

 h /= 60;            // sector 0 to 5
 //i = floor( h );
 i=fasterfloor(h);
 f = h - i;            // factorial part of h
 p = (unsigned char)(v * ( 1 - s ));
 q = (unsigned char)(v * ( 1 - s * f ));
 t = (unsigned char)(v * ( 1 - s * ( 1 - f ) ));

 switch( i ) {
 case 0:
 RGB->r = v;
 RGB->g = t;
 RGB->b = p;
 break;
 case 1:
 RGB->r = q;
 RGB->g = v;
 RGB->b = p;
 break;
 case 2:
 RGB->r = p;
 RGB->g = v;
 RGB->b = t;
 break;
 case 3:
 RGB->r = p;
 RGB->g = q;
 RGB->b = v;
 break;
 case 4:
 RGB->r = t;
 RGB->g = p;
 RGB->b = v;
 break;
 default:        // case 5:
 RGB->r = v;
 RGB->g = p;
 RGB->b = q;
 break;
 }
}

/*******************************************************************************
 * Function RGB_string2RGB_struct
 * Description: Converts an RGB color tring into the individual color values.
 * Copyright 2010 by George Ruinelli
 * Parameters:
 *   1. pointer string with RGB color (source) without leading # i.e. FF0000 for red.
 *   2. pointer tu struct RGB color struct (target)
 * Notes:
 *   - r, g, b values are from 0..255
 ******************************************************************************/
void ICACHE_FLASH_ATTR RGB_string2RGB_struct(char *tuple, struct RGB_set *RGB){
 char tmp[3];

 strncpy(tmp, tuple, 2); tmp[2]= '\0';
 RGB->r=(unsigned char)(strtoul(tmp, NULL, 16));

 strncpy(tmp, &tuple[2], 2); tmp[2]= '\0';
 RGB->g=(unsigned char)(strtoul(tmp, NULL, 16));

 strncpy(tmp, &tuple[4], 2); tmp[2]= '\0';
 RGB->b=(unsigned char)(strtoul(tmp, NULL, 16));
}



// So this code goes off and does a background scan for WIFI and outputs them for me

static void ICACHE_FLASH_ATTR wifi_station_scan_done(void *arg, STATUS status) {
  uint8 ssid[33];
  char tBuf[84];
  char dBuf[84];
  os_sprintf(tBuf, "%s/fromesp/scan", sysCfg.base);

  if (status == OK) {
    struct bss_info *bss_link = (struct bss_info *)arg;

    while (bss_link != NULL) {
      os_memset(ssid, 0, 33);
      if (os_strlen(bss_link->ssid) <= 32) {
        os_memcpy(ssid, bss_link->ssid, os_strlen(bss_link->ssid));
      } else {
        os_memcpy(ssid, bss_link->ssid, 32);
      }
      os_sprintf(dBuf,"WiFi Scan: (%d,\"%s\",%d)\r\n", bss_link->authmode, ssid, bss_link->rssi);
      iprintf(RESPONSE,dBuf);
      os_sprintf(dBuf,"{\"mode\":%d,\"ssid\":\"%s\",\"rssi\":%d}", bss_link->authmode, ssid, bss_link->rssi);
      MQTT_Publish(&mqttClient, tBuf, dBuf, strlen(dBuf), 0, 0);

      bss_link = bss_link->next.stqe_next;
    }
  } else {
  iprintf(RESPONSE,"WIFI scan failed %d\n", status);
  }
}



// This function parses the incoming data, generally from the MQTT - but COULD be from serial
// and processes commands.
//

#define JUSTFORME 2
void ICACHE_FLASH_ATTR mqttDataCb(uint32_t *args, const char* topic, uint32_t topic_len, const char *data, uint32_t data_len)
	{
		char topicBuf[topic_len + 1];
		char dataBuf[data_len + 1];
		char tBuf[84];
		uint8_t messageType;
		heartBeat=TICKS; // reset comms timer - as clearly we have something!!!
		MQTT_Client* client = (MQTT_Client*) args;

		os_memcpy(topicBuf, topic, topic_len);
		topicBuf[topic_len] = 0;

		os_memcpy(dataBuf, data, data_len);
		dataBuf[data_len] = 0;

		messageType = 0;     // my input parser handles multiple commands with up to 6 arguments - the first argument can be a string.
		os_sprintf(tBuf, "%s/toesp", sysCfg.base);
		if (dataBuf[0] == '{') {
			messageType = 1;     // if it has an opening squiggle - type 1
			if (strcmp(topicBuf, tBuf) == 0) messageType = JUSTFORME;     // if it is for me particularly it is a type 2
		}

		if (messageType) {
			char *bufPtr;
			char *tokenPtr;
			char strValue[84];     // could be passing a sizable message
			char strValue2[84];
			char token[84];        // don't need it this big but reused in EXT
			int32_t intValue;
			int32_t arg1;
			int32_t arg2;
			int32_t arg3;
			int32_t arg4;
			int32_t arg5;
			int32_t arg6;
			int16_t argCount;
			int8_t argType;
			int8_t negative;
			uint8_t doUpdate;
			uint8_t isQuery;
			uint8_t noArgs;
			uint8_t gotNum;
			uint8_t string2;
			bufPtr = dataBuf + 1;
			doUpdate = 0;
			while ((*bufPtr) && (*bufPtr != '}')) {     // Sequence through multiple action/value sets and their associated actions
				strValue[0] = 0;
				token[0] = 0;
				tokenPtr = token;
				intValue = -1;
				arg1 = -1;
				arg2 = -1;
				arg3 = -1;
				arg4 = -1;
				arg5 = -1;
				arg6 = -1;
				argCount = 0;
				isQuery = 0;
				noArgs = 0;
				string2=0;
				while (*bufPtr == ' ')
					bufPtr++;
				while ((*bufPtr) && (*bufPtr != ':') && (*bufPtr != ';') && (*bufPtr != '=') && (*bufPtr != '}')) {     // firstly get a token

					if ((*bufPtr == '\'') || (*bufPtr == '"')) bufPtr++;
					else {
						*tokenPtr++ = *bufPtr++;
						*tokenPtr = 0;     // build up command
					}
				}     // end of get a token
				if (*(tokenPtr - 1) == '?') {
					*(tokenPtr - 1) = 0;
					isQuery = 1;
				}
				else {     // only arguments here if it was NOT a query
					while (*bufPtr == ' ')
						bufPtr++;     // kill whitespace(s)
					if ((*bufPtr == ':') || (*bufPtr == '='))     // should be colon but accept =
					{
						bufPtr++;
						while ((*bufPtr) && (*bufPtr != ';') && (*bufPtr != '}'))     // look for arguments while no semicolon, no end of file and no closing brace
						{
							// now to build up arguments - only one string argument allowed... that goes into stringVar

							if (*bufPtr == '"')     // ***** build up a String?
							{
								bufPtr++;
								if (string2==1) tokenPtr = strValue2; else tokenPtr = strValue;    // re-use pointer
								while ((*bufPtr) && (*bufPtr != '"')) {
									if (*bufPtr == '\\') ++bufPtr;
									*tokenPtr++ = *bufPtr++;
									*tokenPtr = 0;
								}
								string2=1;
								bufPtr++;
								argCount++;
								continue;
							}
							// wasn't a string if we got this far, it's a numerical argument
							if (*bufPtr == '-') {     // check for negative numbers
								negative = 1;
								bufPtr++;
							}
							else negative = 0;
							intValue = 0;
							uint8_t hexType;

							if ((*bufPtr == '0') && (*(bufPtr + 1) == 'x')) {
								bufPtr += 2;
								hexType = 1;
							}
							else hexType = 0;     // Here we see if this is a hex number
							// now to collect the decimal or hex number
							gotNum = 0;
							if (hexType == 0) while ((*bufPtr >= '0') && (*bufPtr <= '9')) {
								gotNum = 1;
								intValue *= 10;
								intValue += (*bufPtr++ - '0');
							}
							else while (((*bufPtr >= '0') && (*bufPtr <= '9')) || ((*bufPtr >= 'a') && (*bufPtr <= 'f'))) {
								gotNum = 1;
								intValue *= 16;
								if ((*bufPtr >= 'a') && (*bufPtr <= 'f')) intValue += (*bufPtr++ - 'a' + 10);
								else intValue += (*bufPtr++ - '0');
							}
							if (gotNum == 0) {
								bufPtr++;
								continue;
							}
							if (negative) intValue = -intValue;     // at this point we have a number in intValue
							argCount++;
							switch (argCount)
							{
							case 1:
								arg1 = intValue;
								break;
							case 2:
								arg2 = intValue;
								break;
							case 3:
								arg3 = intValue;
								break;
							case 4:
								arg4 = intValue;
								break;
							case 5:
								arg5 = intValue;
								break;
							case 6:
								arg6 = intValue;
								break;
							}
						}     // end of looping through arguments
					}
					else bufPtr++;
				}     // end of checking for arguments - now to process this command

				if ((*bufPtr == ';') || (*bufPtr == '}') || (*bufPtr == ' ')) bufPtr++;

				// firstly set up the return token.... for return ESP messages
				os_sprintf(tBuf, "%s/fromesp/%s", sysCfg.base,token);

				if (strcmp(token, "ext") == 0) {
					char *gotat = strchr(strValue, '~');
					if (gotat != NULL) {
						*gotat = 0;     // separate the strings
						strcpy(token, strValue);
						os_sprintf(&token[gotat - strValue], "%lu", myRtc);
						gotat++;
						strcat(token, gotat);
						strcpy(strValue, token);
					}
					iprintf(RESPONSE,"{%s}\r\n", strValue);
				}

				else if (strcmp(token, "scan") == 0) {
				    wifi_station_scan(NULL, wifi_station_scan_done);
				}

				else if (strcmp(token, "nextion") == 0) {
					char *gotat = strchr(strValue, '~');
					if (gotat != NULL) {
						*gotat = 0;     // separate the strings
						strcpy(token, strValue);
						os_sprintf(&token[gotat - strValue], "%lu", myRtc);
						gotat++;
						strcat(token, gotat);
						strcpy(strValue, token);
					}
					iprintf(RESPONSE,"\xff\xff\xff%s\xff\xff\xff", strValue);
				}

				else if (strcmp(token, "temp_type") == 0) {  // 0=dallas, 1=DTH22 2=DHT11 3=OUTPUT
					if (sysCfg.sensor != intValue) sysCfg.sensor = intValue;
					if (intValue==3)
						{
							easygpio_pinMode(2, EASYGPIO_PULLUP, EASYGPIO_OUTPUT);
							(sysCfg.invert&2) ? easygpio_outputSet(2, ((sysCfg.out2Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(2, ((sysCfg.out2Status==1) ? OUT_ON : OUT_OFF));
						}
					else easygpio_pinMode(2, EASYGPIO_PULLUP, EASYGPIO_INPUT);
					doUpdate = 1;  ok();
				}

				else if (strcmp(token, "time") == 0) {
					if (!isQuery)
						{
							tm.Valid = 86400;
							myRtc = intValue;
							iprintf(RESPONSE,"{time:%ld}\r\n",myRtc);
						}
					else
						{
					iprintf(RESPONSE,"Time: %02d:%02d:%02d %02d/%02d/%02d\r\n", tm.Hour, tm.Minute, tm.Second,tm.Day, tm.Month, tm.Year);
						}
				}
				else if (strcmp(token, "dawn") == 0) { sysCfg.dawn = intValue; iprintf(RESPONSE,"{dawn:%d}\r\n",sysCfg.dawn); }
				else if (strcmp(token, "dusk") == 0) { sysCfg.dusk = intValue; iprintf(RESPONSE,"{dusk:%d}\r\n",sysCfg.dusk); }

				else if (strcmp(token, "peak") == 0) {
					if (sysCfg.peak != intValue) {
						sysCfg.peak = intValue;
						doUpdate = 1; ok();
					}
				}
				else if (strcmp(token, "off_peak") == 0) {
					if (sysCfg.offPeak != intValue) {
						sysCfg.offPeak = intValue;
						doUpdate = 1; ok();
					}
				}
				else if (strcmp(token, "frost") == 0) {
					if (sysCfg.frost != intValue) {
						sysCfg.frost = intValue;
						doUpdate = 1; ok();
					}
				}
				else if (strcmp(token, "on_1") == 0) {
					if (sysCfg.onOne != intValue) {
						sysCfg.onOne = intValue;
						doUpdate = 1; ok();
					}
				}
				else if (strcmp(token, "off_1") == 0) {
					if (sysCfg.offOne != intValue) {
						sysCfg.offOne = intValue;
						doUpdate = 1; ok();
					}
				}

				else if (strcmp(token, "on_2") == 0) {
					if (sysCfg.onTwo != intValue) {
						sysCfg.onTwo = intValue;
						doUpdate = 1; ok();
					}
				}
				else if (strcmp(token, "off_2") == 0) {
					if (sysCfg.offTwo != intValue) {
						sysCfg.offTwo = intValue;
						doUpdate = 1; ok();
					}
				}
				else if (strcmp(token, "enable13") == 0) {
					if (sysCfg.enable13 != intValue) {
						sysCfg.enable13 = intValue;
						doUpdate = 1; ok();
					}
				}

				else if (strcmp(token, "verbose") == 0) {
						enable_debug_messages=INFO | RESPONSE;
				}
				else if (strcmp(token, "no_serial") == 0) {
						enable_debug_messages=0;
				}

				else if (strcmp(token, "reboot_now") == 0) {
						reboot();
				}

				else if (strcmp(token,"heartbeat")==0) {
				iprintf(RESPONSE,"Tick\r\n");
				}

				else if (strcmp(token, "calibrate") == 0) {
					if (isQuery) {
						os_sprintf(strValue, "%d", sysCfg.calibrate);
					}
					else {
						if (sysCfg.calibrate != intValue) {
							sysCfg.calibrate = intValue;
							doUpdate = 1; ok();
							}
						}
				}

				// syscfg.invert
				// bit 0 = out0
				// bit 1 = out2
				// bit 2 = out4
				// bit 3 = out5
				// bit 4 = out12
				// bit 5 = out13
				// bit 6 = out15

				else if (strcmp(token, "invert") == 0) {
					if (isQuery) os_sprintf(strValue, "%d", sysCfg.invert);
					else {
						if (sysCfg.invert != intValue) {
								sysCfg.invert = intValue;
								doUpdate = 1; ok();
							}
					}
				}

				else if (strcmp(token, "mqtt")==0) {
					MQTT_Publish(&mqttClient, strValue, strValue2, strlen(strValue2), 0, 0);
				ok();
				}

				else if (strcmp(token, "set_serial") == 0) {
					if (isQuery) os_sprintf(strValue, "%d",  sysCfg.serial2);
					else
						{
					    sysCfg.serial2=intValue;
						cfgSave();
						system_restart();
						}
					}


				else if (strcmp(token, "to_nextion")==0) {
				if (strlen(strValue)>=2)
						{
						os_sprintf(tBuf, "%s", strValue);
						Softuart_Writeline_Nextion(&softuart2,strValue);
						ok();
						}
				}

				else if (strcmp(token, "debug") == 0) {
					iprintf(RESPONSE,"Time: %02d:%02d:%02d %02d/%02d/%02d Time Code: %lu dusk: %02d:%02d dawn: %02d:%02d\r\n", tm.Hour, tm.Minute, tm.Second,
							tm.Day, tm.Month, tm.Year, myRtc, sysCfg.dusk / 60, sysCfg.dusk % 60, sysCfg.dawn / 60, sysCfg.dawn % 60);
					iprintf(RESPONSE,"On1: %02d:%02d Off1: %02d:%02d On2: %02d:%02d Off2: %02d:%02d Peak: %dc Off-peak: %dc Frost: %dc \r\n", sysCfg.onOne / 60,
							sysCfg.onOne % 60, sysCfg.offOne / 60, sysCfg.offOne % 60, sysCfg.onTwo / 60, sysCfg.onTwo % 60, sysCfg.offTwo / 60, sysCfg.offTwo % 60,
							sysCfg.peak, sysCfg.offPeak, sysCfg.frost);
					iprintf(RESPONSE,"Heap Size= %ld\r\n",system_get_free_heap_size());
					struct ip_info theIp;
					wifi_get_ip_info(0,&theIp);
					iprintf(RESPONSE,"Host: %s\r\nIP = %d:%d:%d:%d\r\n",wifi_station_get_hostname(),theIp.ip.addr&255,(theIp.ip.addr>>8)&255,(theIp.ip.addr>>16)&255,theIp.ip.addr>>24);
					iprintf(RESPONSE,"Device ID:%s \r\n",sysCfg.deviceId);
					iprintf(RESPONSE,"Unit Name:%s \r\n",sysCfg.base);
					iprintf(RESPONSE,"SSID:%s \r\n",sysCfg.stationSsid);
					iprintf(RESPONSE,"Pass:%s \r\n",sysCfg.stationPwd);
					iprintf(RESPONSE,"MQTT Host:%s \r\n",sysCfg.mqttHost);
					iprintf(RESPONSE,"MQTT Port:%d \r\n",sysCfg.mqttPort);
					iprintf(RESPONSE,"MQTT Pass:%s \r\n",sysCfg.mqttPass);
					iprintf(RESPONSE,"OTA Host:%s \r\n",sysCfg.otaHost);
					iprintf(RESPONSE,"OTA Port:%d \r\n",sysCfg.otaPort);
				}
				else if (strcmp(token, "temperature") == 0) {
					if (isQuery) os_sprintf(strValue, "%d", temperature);
					else temperature = intValue;
				}

				else if (strcmp(token, "humidity") == 0) {
					os_sprintf(strValue, "%d", humidity);
				}

				else if (strcmp(token, "adc") == 0) {
					os_sprintf(strValue, "%d", analog);
				}

				else if (strcmp(token, "ver") == 0) {
					os_sprintf(strValue, "%s", SYSTEM_VER);
				}


				else if (strcmp(token, "voltage") == 0) {
					os_sprintf(strValue, "%d.%02d", (analog * 1000 / sysCfg.calibrate) / 100, (analog * 1000 / sysCfg.calibrate) % 100);
				}

				else if (strcmp(token, "id") == 0) {
					if (isQuery) os_sprintf(strValue, "%s",  sysCfg.base);
					else if (strlen(strValue)>=2){
						strcpy(sysCfg.base, strValue);
						doUpdate=1;
					}
					else iprintf(RESPONSE,"Bad ID\r\n");
				}

				else if (strcmp(token, "mist") == 0) {
					mist.device=arg1;
					mist.active=arg2;
					mist.activeCounter=arg2;
					mist.gap=arg3;
					mist.gapCounter=arg3;
					mist.repeats=arg4;
					easygpio_outputSet(arg1, OUT_ON);
				}

				else if (strcmp(token, "desc") == 0) {
					if (isQuery) os_sprintf(strValue, "%s", sysCfg.description);
					else if (strlen(strValue)>=2){
						strcpy(sysCfg.description, strValue);
						doUpdate = 1;
					}
					else iprintf(RESPONSE,"Bad Desc\r\n");
				}

				else if (strcmp(token, "attrib") == 0) {
					if (isQuery) os_sprintf(strValue, "%s",sysCfg.attribute);
					else if (strlen(strValue)>=2){
						strcpy(sysCfg.attribute, strValue);
						doUpdate = 1;
					}
					else iprintf(RESPONSE,"Bad Attrib\r\n");
				}

				else if (strcmp(token,"fixed_ip")==0) {
					struct ip_info info;
					wifi_station_dhcpc_stop();
					IP4_ADDR(&info.ip, arg2, arg3, arg4, arg5);
					IP4_ADDR(&info.gw, arg2, arg3, arg4, arg6);
					IP4_ADDR(&info.netmask, 255, 255, 255, 0);
					wifi_set_ip_info(arg1, &info);
					wifi_softap_dhcps_start();
					ok();
				}

				else if (strcmp(token,"rssi")==0) {
				int16_t rssi;
				rssi=wifi_station_get_rssi();
				os_sprintf(strValue, "%d", rssi);
				}

				else if (strcmp(token,"ip_info")==0) {
			    // this stuff lifted clean from Jeroem Domberg's code here
			    // https://github.com/jeelabs/esp-link/blob/master/esp-link/cgiwifi.c#L558
				      struct ip_info info;
					  if (wifi_get_ip_info(0, &info)) {
						iprintf(RESPONSE, "ip: %d.%d.%d.%d\r\n", IP2STR(&info.ip.addr));
						iprintf(RESPONSE, "netmask: %d.%d.%d.%d\r\n", IP2STR(&info.netmask.addr));
						iprintf(RESPONSE, "gateway: %d.%d.%d.%d\r\n", IP2STR(&info.gw.addr));

					  } else {
						iprintf(RESPONSE, "ip: -none-\r\n");
					  }

					struct station_config stconf;
				    wifi_station_get_config(&stconf);

				    static char *connStatuses[] = { "idle", "connecting", "wrong password", "AP not found",
				                             "failed", "got IP address" };


				    static char *wifiPhy[]  = { 0, "11b", "11g", "11n" };
				    static char *wifiMode[] = { 0, "STA", "AP", "AP+STA" };
				    uint8_t op = wifi_get_opmode() & 0x3;
				    char *mode = wifiMode[op];
				    char *status = "unknown";
				    int st = wifi_station_get_connect_status();
				    if (st >= 0 && st < sizeof(connStatuses)) status = connStatuses[st];
				    int p = wifi_get_phy_mode();
				    char *phy = wifiPhy[p&3];
				    sint8 rssi = wifi_station_get_rssi();
				    if (rssi > 0) rssi = 0;
				    uint8 mac_addr[6];
				    wifi_get_macaddr(0, mac_addr);
				    uint8_t chan = wifi_get_channel();

				    iprintf(RESPONSE,
				    	    "mode: %s\r\nssid: %s\r\nstatus: %s\r\nphy: %s\r\n"
				    	    "rssi: %ddB\r\nmac:%02x:%02x:%02x:%02x:%02x:%02x\r\n",
				    	    mode, (char*)stconf.ssid, status, phy, rssi,
				    	    mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
				    iprintf(RESPONSE,"channel:%d\r\n",(uint16_t) chan);
				}


// end of new stuff

				else if (strcmp(token, "ssid") == 0) {
					if (isQuery) os_sprintf(strValue, "%s",sysCfg.stationSsid);
					else
						{
						strcpy(sysCfg.stationSsid, strValue);
						doUpdate = 1; ok();
						}
				}
				else if (strcmp(token, "pass") == 0) {
					strcpy(sysCfg.stationPwd, strValue);
					doUpdate = 1; ok();
				}


				else if (strcmp(token, "mqtt_host") == 0) {
				if (isQuery) os_sprintf(strValue, "%s",sysCfg.mqttHost);
				else
					{
					strcpy(sysCfg.mqttHost, strValue);
					doUpdate = 1; ok();
					}
				}
				else if (strcmp(token, "mqtt_port") == 0) {
				if (isQuery) os_sprintf(strValue, "%d",sysCfg.mqttPort);
				else
					{
					strcpy(sysCfg.mqttPort, strValue);
					doUpdate = 1; ok();
					}
				}


				else if (strcmp(token, "ota_host") == 0) {
				if (isQuery) os_sprintf(strValue, "%s",sysCfg.otaHost);
				else
					{
					strcpy(sysCfg.otaHost, strValue);
					doUpdate = 1; ok();
					}
				}
				else if (strcmp(token, "ota_port") == 0) {
				if (isQuery) os_sprintf(strValue, "%d",sysCfg.otaPort);
				else
					{
					strcpy(sysCfg.otaPort, strValue);
					doUpdate = 1; ok();
					}
				}


				else if (strcmp(token, "mqtt_user") == 0) {
				if (isQuery) os_sprintf(strValue, "%s",sysCfg.mqttUser);
				else
					{
					strcpy(sysCfg.mqttUser, strValue);
					doUpdate = 1; ok();
					}
				}
				else if (strcmp(token, "mqtt_pass") == 0) {
				if (isQuery) os_sprintf(strValue, "%s",sysCfg.mqttPass);
				else
					{
					strcpy(sysCfg.mqttPass, strValue);
					doUpdate = 1; ok();
					}
				}

				else if (strcmp(token, "in14") == 0) {
					os_sprintf(strValue, "%d", in14Value);
				}

				else if (strcmp(token, "in14_count") == 0) {
					os_sprintf(strValue, "%lu", in14Count);
					in14Count = 0;
				}

				else if (strcmp(token, "in2_count") == 0) {
					os_sprintf(strValue, "%lu", in2Count);
					in2Count = 0;
				}

			    else if (strcmp(token, "rgb") == 0)
				{
			    if (arg1==4) arg1=5; else if (arg1==5) arg1=4;
			    wsBitMask = (uint8_t) arg1;
				rgb.red = arg3;
				rgb.green = arg2;
				rgb.blue = arg4;
				if (arg5==-1) arg5=10;
				if (arg6==-1) arg6=2;
				rgb.rgbnum = arg5;
				rgb.rgbdelay = arg6;
				rgbOnce=1;
				ok();
				}

			    else if (strcmp(token, "kelvin") == 0)
				{
			    if (arg1==4) arg1=5; else if (arg1==5) arg1=4;
			    wsBitMask = (uint8_t) arg1;

			    uint16 kel;
			    kel=arg2;
				if (kel<1000) kel=1000;
				if (kel>40000) kel=40000;
				kel-=1000;
				kel/=500;
				kel*=3;

				rgb.green=read_rom_uint8(kelvin+kel);
				rgb.red=read_rom_uint8(kelvin+kel+1);
				rgb.blue=read_rom_uint8(kelvin+kel+2);

				if (arg3==-1) arg3=10;
				if (arg4==-1) arg4=2;
				rgb.rgbnum = arg3;
				rgb.rgbdelay = arg4;
				rgbOnce=1;
				ok();
				}

			    else if (strcmp(token, "hsv") == 0)
				{
			    struct RGB_set RGB;
			    struct HSV_set HSV;
			    if (arg1==4) arg1=5; else if (arg1==5) arg1=4;
			    wsBitMask = (uint8_t) arg1;
			    HSV.h=arg2;
			    HSV.s=arg3;
			    HSV.v=arg4;
			    HSV2RGB(HSV, &RGB);
				rgb.red = RGB.g;  // don't worry - ws just not right way...
				rgb.green = RGB.r;
				rgb.blue = RGB.b;
				if (arg5==-1) arg5=10;
				if (arg6==-1) arg6=2;
				rgb.rgbnum = arg5;
				rgb.rgbdelay = arg6;
				rgbOnce=1;
				ok();
				}

			    else if (strcmp(token, "rgbgo") == 0)
				{
			    if (arg1==4) arg1=5; else if (arg1==5) arg1=4;
			    wsBitMask = (uint8_t) arg1;
			    if (arg2>300) arg2=300;
				WS2812OutBuffer(rgb.buffer, arg2*3, 1);     // 3 leds in array, number of repetitions
				ok();
				}

			    else if (strcmp(token, "rgbset") == 0)
				{
			    int a;
			    if ((arg1+arg2)>300) iprintf(RESPONSE,"Bad numbers\r\n");
			    else
					{
						a=arg1*3;
						while (a<((arg1*3)+(arg2*3)))
							{
								rgb.buffer[a++]=arg4;
								rgb.buffer[a++]=arg3;
								rgb.buffer[a++]=arg5;
							}
						ok();
					}
				}

			    else if (strcmp(token, "rainbow") == 0)
				{ // number of leds - and the number of repetitions
			    if (arg1==4) arg1=5; else if (arg1==5) arg1=4;
			    wsBitMask = (uint8_t) arg1;
				rgb.rgbnum = arg2;
				rgb.rainbow = arg3;
				if (arg4<5) arg4=5;
				if (argCount>3) os_timer_arm(&ledTimer, arg4, 1);
				ok();
				}

				else if (strcmp(token, "pwm") == 0) {
						pwm_array.red=arg2;
						pwm_array.green=arg1;
						pwm_array.blue=arg3;
						if (donepwm==0) { pwm_init(freq, duty, 3,io_info);  donepwm=1; }
						if ((argCount<4)||((argCount==4)&&(arg4==0)))
						{
							pwm_array.reda=pwm_array.red; pwm_array.greena=pwm_array.green; pwm_array.bluea=pwm_array.blue;
							pwm_set_duty((uint32_t)PWMTable[pwm_array.red], (uint8)0);
							pwm_set_duty((uint32_t)PWMTable[pwm_array.green], (uint8)1);
							pwm_set_duty((uint32_t)PWMTable[pwm_array.blue], (uint8)2);
							pwm_start();
						}
						else
							{
							if (argCount<4) arg4=20;
							os_timer_arm(&ledTimer, arg4, 1);
							}
						ok();
				}


				else if (strcmp(token, "pwmhsv") == 0) {
						struct RGB_set RGB;
						struct HSV_set HSV;
					    HSV.h=arg1;
					    HSV.s=arg2;
					    HSV.v=arg3;
					    HSV2RGB(HSV, &RGB);
						pwm_array.red=RGB.g;
						pwm_array.green=RGB.r;
						pwm_array.blue=RGB.b;
						if (donepwm==0) { pwm_init(freq, duty, 4,io_info);  donepwm=1; }
						if ((argCount<4)||((argCount==4)&&(arg4==0)))
						{
							pwm_array.reda=pwm_array.red; pwm_array.greena=pwm_array.green; pwm_array.bluea=pwm_array.blue;
							pwm_set_duty((uint32_t)PWMTable[pwm_array.red], (uint8)0);
							pwm_set_duty((uint32_t)PWMTable[pwm_array.green], (uint8)1);
							pwm_set_duty((uint32_t)PWMTable[pwm_array.blue], (uint8)2);
							pwm_start();
						}
						else
							{
							if (argCount<4) arg4=20;
							os_timer_arm(&ledTimer, arg4, 1);
							}
						ok();
				}

				else if (strcmp(token, "rgbstop") == 0) {
					rgbPlayBuffer[0]=-1; rgbPlayPtr=0; rgbRecordPtr=0; rgbDoSeq=0;
					ok();
				}

				else if (strcmp(token,"mqtt")==0)
					{
				    if (strlen(strValue) && strlen(strValue2))
				    	MQTT_Publish(&mqttClient, strValue, strValue2, strlen(strValue2), 0, 0);
					}

				else if (strcmp(token, "rgbstart") == 0) {
			    if (arg1==4) arg1=5; else if (arg1==5) arg1=4;

				    wsBitMask = (uint8_t) arg1;
					rgbPlayPtr=0;
					rgbRecordPtr=0;
					rgbDoSeq=1; rgbTotal=arg2;
					rgbPlayBuffer[0]=-1;
					if (rgbTotal>300) rgbTotal=300;
			        int a=0;
					while (a<(rgbTotal*3))
						{
						rgb.buffer[a++]=0;
						}
					WS2812OutBuffer(rgb.buffer, rgbTotal*3, 1);
					ok();
				}

				else if (strcmp(token, "rgbadd") == 0) {
				if ((rgbRecordPtr<(RGBMAX-7))&&((arg1+arg2)<=rgbTotal))
					{
						rgbPlayBuffer[rgbRecordPtr++] = arg1;
						rgbPlayBuffer[rgbRecordPtr++] = arg2;
						rgbPlayBuffer[rgbRecordPtr++] = arg3;
						rgbPlayBuffer[rgbRecordPtr++] = arg4;
						rgbPlayBuffer[rgbRecordPtr++] = arg5;
						rgbPlayBuffer[rgbRecordPtr++] = arg6;
						rgbPlayBuffer[rgbRecordPtr] = -1;
						ok();
					}
				else
						if ((arg1+arg2)> rgbTotal)iprintf(RESPONSE,"Beyond strip maximum\r\n"); else iprintf(RESPONSE,"RGB buffer full\r\n");
				}

				else if (strcmp(token, "out0") == 0) {
					if (isQuery) os_sprintf(strValue, "%s", (sysCfg.out0Status == 0) ? "OFF" : "ON");
					else {
						if (sysCfg.out0Status != intValue) {
							sysCfg.out0Status = arg1;
							if (sysCfg.out0Status==6) out0Timer=arg2;
							doUpdate = 1;
						}

						(sysCfg.invert&1) ? easygpio_outputSet(0, ((sysCfg.out0Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(0, ((sysCfg.out0Status==1) ? OUT_ON : OUT_OFF));
						 ok();
					}
				}

				else if ((strcmp(token, "out5") == 0)&&(sysCfg.serial2==0)) { // - chip pins reversed
					if (isQuery) os_sprintf(strValue, "%s", (sysCfg.out4Status == 0) ? "OFF" : "ON");
					else
					{
						if (sysCfg.out4Status != intValue) {
							sysCfg.out4Status = intValue;
							doUpdate = 1;
						}     // only update if actual change
						(sysCfg.invert&8) ? easygpio_outputSet(4, ((sysCfg.out4Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(4, ((sysCfg.out4Status==1) ? OUT_ON : OUT_OFF));
						ok();      //
					}
				}

				else if ((strcmp(token, "out4") == 0)&&(sysCfg.serial2==0)) { // - chip pins reversed
					if (isQuery) os_sprintf(strValue, "%s", (sysCfg.out5Status == 0) ? "OFF" : "ON");
					else {
						if (sysCfg.out5Status != intValue) {
							sysCfg.out5Status = intValue;
							doUpdate = 1;
						}     // only update if actual change
						(sysCfg.invert&4) ? easygpio_outputSet(5, ((sysCfg.out5Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(5, ((sysCfg.out5Status==1) ? OUT_ON : OUT_OFF));
					}
				}

				else if (strcmp(token, "out15") == 0) {
					if (isQuery) os_sprintf(strValue, "%s", (sysCfg.out15Status == 0) ? "OFF" : "ON");
					else {
						if (sysCfg.out15Status != intValue) {
							sysCfg.out15Status = intValue;
							doUpdate = 1;
						}     // only update if actual change
						(sysCfg.invert&64) ? easygpio_outputSet(15, ((sysCfg.out15Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(15, ((sysCfg.out15Status==1) ? OUT_ON : OUT_OFF));
					}
				}

				else if (strcmp(token, "out2") == 0) {
					if (isQuery) os_sprintf(strValue, "%s", (sysCfg.out2Status == 0) ? "OFF" : "ON");
					else if (sysCfg.sensor==3) {
						if (sysCfg.out2Status != intValue) {
							sysCfg.out2Status = intValue;
							doUpdate = 1;
						}     // only update if actual change
						(sysCfg.invert&2) ? easygpio_outputSet(2, ((sysCfg.out2Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(2, ((sysCfg.out2Status==1) ? OUT_ON : OUT_OFF));
					}
				}

				else if (strcmp(token, "out12") == 0) {
					if (isQuery) os_sprintf(strValue, "%s", (sysCfg.out12Status == 0) ? "OFF" : "ON");
					else {
						if (sysCfg.out12Status != intValue) {
							sysCfg.out12Status = intValue;
							doUpdate = 1;
						}     // only update if actual change
						(sysCfg.invert&16) ? easygpio_outputSet(12, ((sysCfg.out12Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(12, ((sysCfg.out12Status==1) ? OUT_ON : OUT_OFF));
					}
				}

				else if ((strcmp(token, "out13") == 0) && (sysCfg.enable13)) {
					if (isQuery) os_sprintf(strValue, "%s", (sysCfg.out13Status == 0) ? "OFF" : "ON");
					else {
						if (sysCfg.out13Status != intValue) {
							sysCfg.out13Status = intValue;
							doUpdate = 1;
						}     // only update if actual change
						(sysCfg.invert&32) ? easygpio_outputSet(13, ((sysCfg.out13Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(13, ((sysCfg.out13Status==1) ? OUT_ON : OUT_OFF));
					}
				}

				else if (strcmp(token, "flashsize") == 0) {
				  uint32_t id = spi_flash_get_id();
				  uint8_t mfgr_id = id & 0xff;
				  uint8_t type_id = (id >> 8) & 0xff; // not relevant for size calculation
				  uint8_t size_id = (id >> 16) & 0xff; // lucky for us, WinBond ID's their chips in a form that lets us calculate the size
				  if(mfgr_id != 0xEF) // 0xEF is WinBond; that's all we care about (for now)
				  iprintf(RESPONSE,"Not Winbond!\r\n");
				  iprintf(RESPONSE,"Manuf ID= %d, Type ID= %d, Size=%ld\r\n",(uint16_t) mfgr_id, (uint16_t) type_id, 1 << size_id );
				}

				else if (strcmp(token, "reset") == 0) {
					cfgSave();
					system_restart();
				}

				else if (strcmp(token, "clock") == 0) {
					if (isQuery) iprintf(RESPONSE,"Clock=%d\r\n", sysCfg.clock);
					else {
							if (sysCfg.clock!=255)
							{
						       for (int a=0;a<180;a+=3)
									{
										rgb.buffer[a]=0;
										rgb.buffer[a+1]=0;
										rgb.buffer[a+2]=0;
									}
							    WS2812OutBuffer(rgb.buffer, 180,1); // 60 LEDs
							}
						sysCfg.clock=intValue;
						doUpdate = 1;
					      }
				}

				else if (strcmp(token, "in_bounce") == 0) {
					sysCfg.in14Bounce = intValue;
					doUpdate = 1; ok();
				}

				else if (strcmp(token,"reconnectMQTT")==0) {
				MQTT_Connect(&mqttClient);
				ok();
				}

				else if (strcmp(token, "otaupdate") == 0) {
					if (system_get_flash_size_map() >= FLASH_SIZE_8M_MAP_512_512) {
						OtaUpdate();
					} else {
						iprintf(RESPONSE, "Not supported on this device\r\n");
					}
				}

				else if (strcmp(token, "romswitch") == 0) {
					if (system_get_flash_size_map() >= FLASH_SIZE_8M_MAP_512_512) {
						uint8 before, after;
						before = rboot_get_current_rom();
						if (before == 0) after = 1; else after = 0;
						iprintf(INFO, "Swapping from rom %d to rom %d.\r\n", before, after);
						rboot_set_current_rom(after);
						iprintf(INFO, "Restarting...\r\n\r\n");
						system_restart();
					} else {
						iprintf(RESPONSE, "Not supported on this device\r\n");
					}
				}

				else if (strcmp(token, "override") == 0) {
					sysCfg.override0 = intValue;
					if (intValue == 0) easygpio_outputSet(0, ((sysCfg.out0Status==1) ? OUT_ON : OUT_OFF));     //restore
					doUpdate = 1; ok();
				}

				else if (strcmp(token, "override_time") == 0) {
					sysCfg.override0Time = (uint32_t) intValue * 60 * 20;
					doUpdate = 1; ok();
				}

				else if (strcmp(token, "gettime") == 0) {
					uint32 current_stamp;
					current_stamp=sntp_get_current_timestamp();
                    if (current_stamp) current_stamp-=(60*60*7);
					os_sprintf(strValue, "%d %s \r\n",current_stamp, sntp_get_real_time(current_stamp));
				}
				else {
				iprintf(RESPONSE,"Eh?\r\n");
				}

				if (isQuery) { // saves repeating this over and over in commands
					if (messageType == JUSTFORME)
						MQTT_Publish(client, tBuf, strValue, strlen(strValue), 0, 0);
					else
						iprintf(RESPONSE,"%s=%s\r\n", tBuf,strValue);
				}

			}     // loop through multiple messages before initiating a single possible save (minimise writes)
			if (doUpdate == 1) cfgSave();
		}
	}

// Input a value 0 to 255 to get a colour value.
LOCAL ICACHE_FLASH_ATTR void colourWheel(uint8_t WheelPos)
	{
		if (WheelPos < 85) {
			rgb.reda = (uint8_t) (WheelPos * 3);
			rgb.greena = (uint8_t) (255 - WheelPos * 3);
			rgb.bluea = 0;
		}
		else if (WheelPos < 170) {
			WheelPos -= 85;
			rgb.reda = (uint8_t) (255 - WheelPos * 3);
			rgb.greena = 0;
			rgb.bluea = (uint8_t) (WheelPos * 3);

		}
		else {
			WheelPos -= 170;
			rgb.reda = 0;
			rgb.greena = (uint8_t) (WheelPos * 3);
			rgb.bluea = (uint8_t) (255 - WheelPos * 3);
		}
	}

LOCAL void ICACHE_FLASH_ATTR startup_cb(void *arg)
	{
    startupTimeout++;
    if (!easygpio_inputGet(2)) { sysCfg.wifiSetup+=1; }
    if (startupTimeout>14) {
    						if ((easygpio_inputGet(2))&&(sysCfg.wifiSetup==0)) os_timer_disarm(&startupTimer);
    						if (sysCfg.wifiSetup>4)
    							{
								if (easygpio_inputGet(2)) { sysCfg.wifiSetup=1; cfgSave(); system_restart(); }
								else
								easygpio_outputSet(13, 1);
    							}
    						else sysCfg.wifiSetup=0;
    						}
    else { if (startupTimeout&1) easygpio_outputSet(13, 1); else easygpio_outputSet(13, 0);}
}

LOCAL void ICACHE_FLASH_ATTR led_cb(void *arg)
	{
	if ((pwm_array.reda!=pwm_array.red)||(pwm_array.greena!=pwm_array.green)||(pwm_array.bluea!=pwm_array.blue))
		{
 			if (pwm_array.reda > pwm_array.red) pwm_array.reda--;
			if (pwm_array.reda<pwm_array.red) pwm_array.reda++;
			if (pwm_array.greena > pwm_array.green) pwm_array.greena--;
			if (pwm_array.greena<pwm_array.green) pwm_array.greena++;
			if (pwm_array.bluea > pwm_array.blue) pwm_array.bluea--;
			if (pwm_array.bluea<pwm_array.blue) pwm_array.bluea++;
			if (donepwm==0) { pwm_init(freq, duty, 3,io_info);  donepwm=1; }
			pwm_set_duty((uint32_t)PWMTable[pwm_array.reda], (uint8)0);
			pwm_set_duty((uint32_t)PWMTable[pwm_array.greena], (uint8)1);
			pwm_set_duty((uint32_t)PWMTable[pwm_array.bluea], (uint8)2);
			pwm_start();
		}

//Handle Serial LEDS on GPIO12

		if (rgb.rainbow) {

			rgb.rainbow--;
			if (rgb.rainbow == 0) {
				rgb.red = 0;
				rgb.green = 0;
				rgb.blue = 0;
				rgb.rgbdelay = 5;
			}
			else {
				colourWheel((uint8_t) (rgb.rainbow & 255));
				rgb.buffer[0] = rgb.reda;
				rgb.buffer[1] = rgb.greena;
				rgb.buffer[2] = rgb.bluea;
				WS2812OutBuffer(rgb.buffer, 3, rgb.rgbnum);     // 3 leds in array, number of repetitions
			}
		}

		else
		/// rgb to fade from any colour to any other colour for any number of LEDS for any given period in secs
		if ((rgb.red != rgb.reda) || (rgb.green != rgb.greena) || (rgb.blue != rgb.bluea) || (rgbOnce)) {
            rgbOnce=0;
			if (rgb.reda < rgb.red) rgb.reda += ((rgb.red - rgb.reda) / (rgb.rgbdelay * 20)) + 1;
			if (rgb.greena < rgb.green) rgb.greena += ((rgb.green - rgb.greena) / (rgb.rgbdelay * 20)) + 1;
			if (rgb.bluea < rgb.blue) rgb.bluea += ((rgb.blue - rgb.bluea) / (rgb.rgbdelay * 20)) + 1;
			if (rgb.reda > rgb.red) rgb.reda -= ((rgb.reda - rgb.red) / (rgb.rgbdelay * 20)) + 1;
			if (rgb.greena > rgb.green) rgb.greena -= ((rgb.greena - rgb.green) / (rgb.rgbdelay * 20)) + 1;
			if (rgb.bluea > rgb.blue) rgb.bluea -= ((rgb.bluea - rgb.blue) / (rgb.rgbdelay * 20)) + 1;

			if (rgb.rgbnum == 0) {
				rgb.rgbnum = 1;
				rgb.reda = rgb.red;
				rgb.greena = rgb.green;
				rgb.bluea = rgb.blue;
			}     // instant
			//rgb.buffer[0] = ledTable[rgb.reda];
			//rgb.buffer[1] = ledTable[rgb.greena];
			//rgb.buffer[2] = ledTable[rgb.bluea];

			rgb.buffer[0] = read_rom_uint8(ledTable+rgb.reda);
			rgb.buffer[1] = read_rom_uint8(ledTable+rgb.greena);
			rgb.buffer[2] = read_rom_uint8(ledTable+rgb.bluea);


			WS2812OutBuffer(rgb.buffer, 3, rgb.rgbnum);     // 3 leds in array, number of repetitions
		}

	}

//*** Bounce timer. This is 25ms period.
LOCAL void ICACHE_FLASH_ATTR bounce_cb(void *arg)
	{
		int8_t pin14changed;
		int8_t pin2changed;
		char tBuf[84];
		char pBuf[84];
		pin14changed = 0;
		pin2changed=0;


		// experiment - read Nextion data at 56k
		char nextion_buffer[96];
		if (Softuart_Readline_Nextion(&softuart2,nextion_buffer,95)>0)
				{
				//iprintf(RESPONSE,nextion_buffer+1);

				char *np;
				char ncount;
				ncount=0;
				np=nextion_buffer+1;
				while ((ncount<83)&& (*np) && (*np!='~')) { tBuf[ncount++]=*np++; tBuf[ncount]=0; }
				if (*np=='~')
					{
				    ncount=0; np++;
				    while ((ncount<83)&& (*np)) { pBuf[ncount++]=*np++; pBuf[ncount]=0; }
				    if (*np==0)
				    	{
				    	MQTT_Publish(&mqttClient, tBuf, pBuf, strlen(pBuf), 0, 0);
				    	}
					}
				}


        // flashing light


		if (connectStatus&16) // ie once started and got signal
		{
			if (sysCfg.enable13 == 0) { // only if 13 not used elsewhere
					if (state13 == 1) {
						if (e13OnTime)
							{
							if (--e13OnTime==0)
								{
								e13OnTime=e13OnSet;
								easygpio_outputSet(13, OUT_OFF);
								state13 = 0;
								}
							}
					}
					else {
					if (e13OffTime)
							{
							if (--e13OffTime==0)
								{
								e13OffTime=e13OffSet;
								easygpio_outputSet(13, OUT_ON);
								state13 = 1;
								}
							}
					}
			}
		}

		if (gotSerial) {
			gotSerial = 0;
			mqttDataCb(NULL, "toesp", 5, serialInBuf, strlen(serialInBuf));
		}

		if (in2BounceCount > sysCfg.in14Bounce) {
			in2BounceCount = 0;
			in2Value = easygpio_inputGet(2);
			// send new value...

			os_sprintf(tBuf, "%s/fromesp/in2", sysCfg.base);
			if (in2Value) MQTT_Publish(&mqttClient, tBuf, "1", 1, 0, 0);
			else MQTT_Publish(&mqttClient, tBuf, "0", 1, 0, 0);

			if (in2Value == 0) {
				in2Count++;
				pin2changed = -1;
			}
			else {
				pin2changed = 1;
			}
		}
		if (in2Value != easygpio_inputGet(2)) in2BounceCount++;
		else in2BounceCount = 0;

		if (inBounceCount > sysCfg.in14Bounce) {
			inBounceCount = 0;
			in14Value = easygpio_inputGet(14);
			// send new value...

			os_sprintf(tBuf, "%s/fromesp/in1", sysCfg.base);
			if (in14Value) MQTT_Publish(&mqttClient, tBuf, "1", 1, 0, 0);
			else MQTT_Publish(&mqttClient, tBuf, "0", 1, 0, 0);

			if (in14Value == 0) {
				in14Count++;
				pin14changed = -1;
			}
			else {
				pin14changed = 1;
			}
		}
		if (in14Value != easygpio_inputGet(14)) inBounceCount++;
		else inBounceCount = 0;

		// now check for manual override on output 0
		if (sysCfg.override0) {
			if (pin14changed == 1) {
				pinChangeDownCounter = sysCfg.override0Time;
				easygpio_outputSet(0, 1);
			}     // overwrite output 0 with button
			if (pin14changed == -1) {
				pinChangeDownCounter = sysCfg.override0Time;
				easygpio_outputSet(0, 0);
			}
		}
		if (pinChangeDownCounter) {
			if (--pinChangeDownCounter == 0) GPIO_OUTPUT_SET(GPIO_0, ((sysCfg.out0Status==1) ? OUT_ON : OUT_OFF));
		}
	}


LOCAL void ICACHE_FLASH_ATTR temperature_cb(void *arg)
	{
		if ((sysCfg.sensor == 1) || (sysCfg.sensor == 2)) readDHT();
		else if (sysCfg.sensor == 0) {
		    humidity=0;
			ds_reset();
			ds_write(0xcc, 1);
			ds_write(0xbe, 1);
			temperature = (int) ds_read();
			temperature = temperature + (int) ds_read() * 256;
			temperature /= 16;
			if (temperature > 100) temperature -= 4096;
			ds_reset();
			ds_write(0xcc, 1);
			ds_write(0x44, 1);
		}
		if (gotDsReading == 0) temperature = 0;     // ignore first reading
		gotDsReading = 1;
	}

LOCAL int ICACHE_FLASH_ATTR mod(int a, int b)
{
   int ret = a % b;
   if(ret < 0)
     ret+=b;
   return ret;
}

LOCAL void ICACHE_FLASH_ATTR clock_cb(void *arg)
{
if (sysCfg.clock!=255) // fancy clock on a port
	{
        wsBitMask = (uint8_t) sysCfg.clock;
		int hour12=(tm.Hour%12)*5;

        int a;
        for (a=0;a<178;a+=3)
			{
				rgb.buffer[a]=0;
				rgb.buffer[a+1]=0;
				rgb.buffer[a+2]=0;
			}

		rgb.buffer[mod(tm.Second*3+2,180)]=255; // seconds hand blue
		rgb.buffer[mod(tm.Minute*3-3,180)]=8; // minutes green
		rgb.buffer[mod(tm.Minute*3,180)]=255; // minutes green
		rgb.buffer[mod(tm.Minute*3+3,180)]=8; // minutes green

		int x=((hour12)+(tm.Minute/12))*3;

		rgb.buffer[mod(x-8,180)]=5;
		rgb.buffer[mod(x-5,180)]=16;
		rgb.buffer[mod(x-2,180)]=80;
		rgb.buffer[mod(x+1,180)]=255;
		rgb.buffer[mod(x+4,180)]=80;
		rgb.buffer[mod(x+7,180)]=16;
		rgb.buffer[mod(x+10,180)]=5;

		WS2812OutBuffer(rgb.buffer, 180,1); // 60 LEDs
	}
}

LOCAL void ICACHE_FLASH_ATTR rgb_cb(void *arg)
{
 int a,b,c,d,e,f,g;
 if (rgbPlayBuffer[rgbPlayPtr]==-1) rgbPlayPtr=0;
 if ((rgbPlayBuffer[rgbPlayPtr]!=-1)&&(rgbDoSeq))
 {
	{
			b=rgbPlayBuffer[rgbPlayPtr++];
			c=rgbPlayBuffer[rgbPlayPtr++];
			d=rgbPlayBuffer[rgbPlayPtr++];
			e=rgbPlayBuffer[rgbPlayPtr++];
			f=rgbPlayBuffer[rgbPlayPtr++];
			g=rgbPlayBuffer[rgbPlayPtr++]; if (g<5) g=5;
			a=b*3;
			while (a<((b*3)+(c*3)))
				{
					rgb.buffer[a++]=e;
					rgb.buffer[a++]=d;
					rgb.buffer[a++]=f;
				}
	}
    WS2812OutBuffer(rgb.buffer,rgbTotal*3,1);
	os_timer_arm(&rgbTimer, g, 1);
 }
 else os_timer_arm(&rgbTimer, 250, 1); // tickover if not running
}

void ICACHE_FLASH_ATTR reboot(void)
{
	easygpio_pinMode(16, EASYGPIO_NOPULL, EASYGPIO_OUTPUT);
	easygpio_outputSet(16,1);
	system_restart(); // just in case - ESP-01 etc with no GPIO16 or not connected
}

// handling power up - and lost signal - also handling the flashing light...

LOCAL void ICACHE_FLASH_ATTR lostThePlot_cb(void *arg)
{
	struct ip_info theIp;
	if (connectStatus&16) // if not true - don't do anything - Aidan is handling setup
	{
		if (heartBeat==0) reboot();
		iprintf(INFO,"WDT:%d\r\n",heartBeat);
		wifi_get_ip_info(0,&theIp);
		if (theIp.ip.addr)
				{
		if (tm.Valid) {	e13OnSet=1;e13OffSet=120; } else {	e13OnSet=1;e13OffSet=10; }
				connectStatus|=9;
				if ((connectStatus&2)==0) { iprintf(INFO,"Timers initialised - "); connectStatus|=2; mqtt_init(); } // start up timers
				if ((connectStatus&4)==0) {	iprintf(INFO,"MQTT connecting - "); MQTT_Connect(&mqttClient); }
				iprintf(RESPONSE,"%s OK\r\n",sysCfg.base);
				}
		else
				{
				if (connectStatus&32)
					{  connectStatus&=(255-32); os_timer_arm(&lostThePlotTimer, 4000, 1); iprintf(RESPONSE,"Speeding up Timer\r\n"); }
		        e13OnSet=10;e13OffSet=10;
		        connectStatus &=(255-1);
				if (connectStatus&4) { iprintf(INFO,"MQTT disconnected - "); connectStatus&=(255-4); MQTT_Disconnect(&mqttClient); }
				if (connectStatus&8) iprintf(RESPONSE,"LOST %s\r\n",sysCfg.stationSsid); else iprintf(RESPONSE,"WAITING FOR %s\r\n",sysCfg.stationSsid);
				}
//		if ((heartBeat==TICKS) && ((connectStatus&32)==0))
//			{ connectStatus|=32; os_timer_arm(&lostThePlotTimer, 15000, 1);  }
		if (heartBeat) heartBeat--;

	}
}

LOCAL void ICACHE_FLASH_ATTR rtc_cb(void *arg)
	{
	   ++myRtc;

	   if (mist.repeats)
	   {
	   if (mist.activeCounter)
		   {
			mist.activeCounter--;
			if (mist.activeCounter==0) { mist.gapCounter=mist.gap; easygpio_outputSet(mist.device, OUT_OFF); }
		   }
	   else if (mist.gapCounter)
		   {
			mist.gapCounter--;
			if (mist.gapCounter==0) { mist.repeats--; if (mist.repeats) { mist.activeCounter=mist.active; easygpio_outputSet(mist.device, OUT_ON); }}
		   }
	   }

		if (tm.Valid) {
			timeTimeout = 0;
			tm.Valid--;
		}

		if (tm.Valid < 1000)     // no valid time? Time is sent on powerup (mqttConnect) and also every 24 hours so unit has dawn dusk info
		{
			if (timeTimeout++ == 120)     // 2 after minutes of no time ask for it every 2 mins. Should never be needed
			{
				char tBuf[TBUFSIZE];
				timeTimeout = 0;
				os_sprintf(tBuf, "{\"id\":\"%s\",\"desc\":\"%s\",\"attribute\":\"%s\",\"fail\":\"%d\"}",
					    sysCfg.base, sysCfg.description, sysCfg.attribute, sysCfg.lastFail); // changed to json
				MQTT_Publish(&mqttClient, "esplogon", tBuf, strlen(tBuf), 0, 0);
			}
		}

		convertTime();
		analogAccum=((analogAccum*9)/10)+system_adc_read();
		analog=analogAccum/10;

		if (tm.Second == 0)     // **** timed once every minute handles sysCfg.out_status
		{
			// need temporary minutes in the day
			int t_mins, doit;
			t_mins = tm.Hour;
			t_mins *= 60;
			t_mins += tm.Minute;
			switch (sysCfg.out0Status)
			{
			case 0:
				break;     // covered in the OUT0 command
			case 1:
				if (tm.Valid == 0)
				easygpio_outputSet(0, (sysCfg.invert));
				break;     // covered in the OUT0 command - but if no time available, turn OUT0 off after a day
			case 2:
				if (t_mins > sysCfg.dusk) // 2 means on after dusk
				easygpio_outputSet(0, ((sysCfg.invert&1) ^ 1));
				else
				easygpio_outputSet(0, (sysCfg.invert&1));
				break;
			case 3:
				if ((t_mins > sysCfg.dusk) || (t_mins < sysCfg.dawn)) // 3 means on all night
				easygpio_outputSet(0, ((sysCfg.invert&1) ^ 1));
				else
				easygpio_outputSet(0, (sysCfg.invert&1));
				break;
			case 4:
				if ((t_mins > sysCfg.dawn) && (t_mins < sysCfg.dusk)) // 4 means on all day
				easygpio_outputSet(0, ((sysCfg.invert&1) ^ 1));
				else
				easygpio_outputSet(0, (sysCfg.invert&1));
				break;
			case 5: // heating control
				doit = 0;
				if (sysCfg.onOne > sysCfg.offOne) {
					if ((t_mins > sysCfg.onOne) || (t_mins < sysCfg.offOne)) doit = 1;
				}
				else {
					if ((t_mins > sysCfg.onOne) && (t_mins < sysCfg.offOne)) doit = 1;
				}
				if (sysCfg.onOne > sysCfg.offTwo) {
					if ((t_mins > sysCfg.onTwo) || (t_mins < sysCfg.offTwo)) doit = 1;
				}
				else {
					if ((t_mins > sysCfg.onTwo) && (t_mins < sysCfg.offTwo)) doit = 1;
				}

				if (doit) {
					if (temperature < sysCfg.peak)
					easygpio_outputSet(0, ((sysCfg.invert&1) ^ 1));
					else
					easygpio_outputSet(0, (sysCfg.invert&1));
				}
				else     // if not peak - OR if the time is not set due to bad connection, say  -default to off-peak
				{
					if (temperature < sysCfg.offPeak)
					easygpio_outputSet(0, ((sysCfg.invert&1) ^ 1));
					else
					easygpio_outputSet(0, (sysCfg.invert&1));

				}
				break;
			case 6: // simple timer in minutes....passed as {out0,6,100} ie on for up to 100 minutes...
				if (out0Timer)
				{
				easygpio_outputSet(0, ((sysCfg.invert&1) ^ 1));
				out0Timer--;
				}
				else easygpio_outputSet(0, (sysCfg.invert&1));

				break;

			default: // from here - heating timeout.... ie from 7 onwards is number of days for standby

				if ((tm.Minute == 0) && (tm.Hour == 0)) {
					sysCfg.out0Status--; // drop down once a day....
					cfgSave();
				}
				// frost setting if over 5
				if (sysCfg.out0Status > 6) {
					if (temperature < sysCfg.frost)
					easygpio_outputSet(0, ((sysCfg.invert&1) ^ 1));
					else
					easygpio_outputSet(0, (sysCfg.invert&1));
				}
				break;
			}
		}
	}

void ICACHE_FLASH_ATTR mqtt_setup()
{
    MQTT_InitConnection(&mqttClient, sysCfg.mqttHost, sysCfg.mqttPort,sysCfg.security);
    MQTT_InitClient(&mqttClient, sysCfg.deviceId, sysCfg.mqttUser,sysCfg.mqttPass, sysCfg.mqttKeepalive, 1);
    MQTT_InitLWT(&mqttClient, "/lwt", "offline", 0, 0);
    MQTT_OnConnected(&mqttClient, mqttConnectedCb);
    MQTT_OnDisconnected(&mqttClient, mqttDisconnectedCb);
    MQTT_OnPublished(&mqttClient, mqttPublishedCb);
    MQTT_OnData(&mqttClient, mqttDataCb);
}

void ICACHE_FLASH_ATTR mqtt_init()
    {

// Set up a timer to read the temperature
    os_timer_disarm(&temperatureTimer);
    os_timer_setfn(&temperatureTimer, (os_timer_func_t *) temperature_cb, (void *) 0);
    os_timer_arm(&temperatureTimer, DELAY, 1);

    os_timer_disarm(&rtcTimer);
    os_timer_setfn(&rtcTimer, (os_timer_func_t *) rtc_cb, (void *) 0);
    os_timer_arm(&rtcTimer, 1000, 1);

	// Set up a led fader timer 20 times a second
	os_timer_disarm(&ledTimer);
	os_timer_setfn(&ledTimer, (os_timer_func_t *) led_cb, (void *) 0);
	os_timer_arm(&ledTimer, 50, 1);

	// my silly LED clock
	os_timer_disarm(&clockTimer);
	os_timer_setfn(&clockTimer, (os_timer_func_t *) clock_cb, (void *) 0);
	//os_timer_arm(&clockTimer, 16, 1);
	os_timer_arm(&clockTimer, 1000, 1);

	// RGB timer
	os_timer_disarm(&rgbTimer);
	os_timer_setfn(&rgbTimer, (os_timer_func_t *) rgb_cb, (void *) 0);
	os_timer_arm(&rgbTimer, 250, 1);

	// off to get the time
	sntp_setservername(0,"us.pool.ntp.org");
	sntp_setservername(1,"ntp.sjtu.edu.cn");
	sntp_init();
	mqtt_setup();
    }


void ICACHE_FLASH_ATTR petes_initialisation(void)
	{
	uart_init(BIT_RATE_115200, BIT_RATE_115200);
	cfgLoad();

// init 4 and 5 only if serial2 is zero

	    easygpio_pinMode(0, EASYGPIO_NOPULL, EASYGPIO_OUTPUT);
	    if (sysCfg.serial2==0) easygpio_pinMode(4, EASYGPIO_NOPULL, EASYGPIO_OUTPUT);
	    if (sysCfg.serial2==0) easygpio_pinMode(5, EASYGPIO_NOPULL, EASYGPIO_OUTPUT);
	    easygpio_pinMode(12, EASYGPIO_NOPULL, EASYGPIO_OUTPUT);
	    easygpio_pinMode(13, EASYGPIO_NOPULL, EASYGPIO_OUTPUT);
		easygpio_pinMode(14, EASYGPIO_PULLUP, EASYGPIO_INPUT);
		easygpio_pinMode(15, EASYGPIO_PULLUP, EASYGPIO_OUTPUT);
		// easygpio_pinMode(16, EASYGPIO_NOPULL, EASYGPIO_OUTPUT); // bad board causes reset - only use on latest


		//init software uart
		if (sysCfg.serial2>0) Softuart_SetPinRx(&softuart2,4);
		if (sysCfg.serial2>0)Softuart_SetPinTx(&softuart2,5);

		//startup
		if (sysCfg.serial2>0)
			{
			if (sysCfg.serial2==3) Softuart_Init(&softuart2,9600); else Softuart_Init(&softuart2,57600);
			Softuart_Writeline_Nextion(&softuart2," ");
			}

		analogAccum=system_adc_read()*10;
		analog=analogAccum/10;

		pwm_array.red=0; pwm_array.reda=0;
		pwm_array.green=0; pwm_array.greena=0;
		pwm_array.blue=0; pwm_array.bluea=0;
		rgbPlayBuffer[0]=-1; rgbPlayPtr=0; rgbRecordPtr=0;

		inBounceCount = 0;

		mist.repeats=0; // special for the likes of garden misters.

		if (sysCfg.sensor==3)
		{
			easygpio_pinMode(2, EASYGPIO_PULLUP, EASYGPIO_OUTPUT);
			(sysCfg.invert&2) ? easygpio_outputSet(2, ((sysCfg.out2Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(2, ((sysCfg.out2Status==1) ? OUT_ON : OUT_OFF));
		}
		else
		easygpio_pinMode(2, EASYGPIO_PULLUP, EASYGPIO_INPUT
		);

		// use 4 and 5 only if serial 2 is zero
		if (sysCfg.serial2==0)  { (sysCfg.invert&4) ? easygpio_outputSet(5, ((sysCfg.out5Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(5, ((sysCfg.out5Status==1) ? OUT_ON : OUT_OFF)); }
		(sysCfg.invert&1) ? easygpio_outputSet(0, ((sysCfg.out0Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(0, ((sysCfg.out0Status==1) ? OUT_ON : OUT_OFF));
		if (sysCfg.serial2==0)  { (sysCfg.invert&8) ? easygpio_outputSet(4, ((sysCfg.out4Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(4, ((sysCfg.out4Status==1) ? OUT_ON : OUT_OFF)); }
		(sysCfg.invert&64) ? easygpio_outputSet(15, ((sysCfg.out15Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(15, ((sysCfg.out15Status==1) ? OUT_ON : OUT_OFF));
		(sysCfg.invert&16) ? easygpio_outputSet(12, ((sysCfg.out12Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(12, ((sysCfg.out12Status==1) ? OUT_ON : OUT_OFF));
		(sysCfg.invert&32) ? easygpio_outputSet(13, ((sysCfg.out13Status==1) ? OUT_OFF : OUT_ON)) :  easygpio_outputSet(13, ((sysCfg.out13Status==1) ? OUT_ON : OUT_OFF));

		iprintf(INFO,"\r\n\r\nESP Starting...\r\n");

		tm.Valid = 0;     //ps time is invalid

		rgb.reda = 0;
		rgb.greena = 0;
		rgb.bluea = 0;
		rgb.red = 0;
		rgb.green = 0;
		rgb.blue = 0;
		rgb.rainbow = 0;

		pinChangeDownCounter = 0;

		// Set up a powerup timer for real time clock 50ms
		os_timer_disarm(&startupTimer);
		os_timer_setfn(&startupTimer, (os_timer_func_t *) startup_cb, (void *) 0);
		os_timer_arm(&startupTimer, 200, 1);

		// Set up a debounce timer 20 times a second
		os_timer_disarm(&bounceTimer);
		os_timer_setfn(&bounceTimer, (os_timer_func_t *) bounce_cb, (void *) 0);
		os_timer_arm(&bounceTimer, 25, 1);

		// Set up a timer with 15 second interval for checking WIFI...
		os_timer_disarm(&lostThePlotTimer);
		os_timer_setfn(&lostThePlotTimer, (os_timer_func_t *) lostThePlot_cb, (void *) 0);
		os_timer_arm(&lostThePlotTimer, 4000, 1); // every 4 secs for WIFI
		timeTimeout = 0;
	}
